#pragma once

#include "Components/ActorComponent.h"
#include "OpenDoor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnOpenRequest);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnCloseRequest);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// Event fired when door should open
	UPROPERTY(BlueprintAssignable)
	FOnOpenRequest OnOpen;

	// Event fired when door should close
	UPROPERTY(BlueprintAssignable)
	FOnOpenRequest OnClose;

private:
	// The volume where some weight needs to be put for the door to open
	UPROPERTY(EditAnywhere)
	ATriggerVolume* PreassurePlate = nullptr;

	// Stores the total mass in kg on the trigger volume preassure plate
	UPROPERTY(EditAnywhere)
	float TotalMass = 50.0f;

	// Delay in seconds to put on the sound and animation
	UPROPERTY(EditAnywhere)
	float Delay = 1.0f;

	float LastTimeDoorOpen = 0.0f;

	// Returns total mass in kg
	float GetTotalMassOfActorsOnPlate();

};
