// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/ActorComponent.h"
#include "Grabber.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ESCAPE_API UGrabber : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UGrabber();

	// Called when the game starts
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void TickComponent( float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction ) override;

	// For reach queries when grabbing
	enum ReachPosition { Start, End };

private:
	// How far ahead of the player can we reach in cm
	float Reach = 100.0f;

	// Pointer to the physics handle
	UPhysicsHandleComponent* PhysicsHandle = nullptr;

	// Holds the input component so we can handle clicks
	UInputComponent* InputComponent = nullptr;

	// Ray-cast and grab what's in reach
	void Grab();

	// Release anything that has been grabbed
	void Release();

	// Finds the attached physics handle component
	void FindPhysicsHandleComponent();

	// Sets up the input component actions
	void SetupInputComponent();

	// Return hit for first physics body in reach
	FHitResult GetFirstPhysicsBodyInReach() const;

	// For getting the reach vectors (origin and tip)
	FVector GetReachLine(ReachPosition) const;

};
